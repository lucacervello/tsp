import it.luca.cervello.Game;
import org.junit.Test;

public class GameTest {

    @Test
    public void aopWorks() {
        Game game = new Game();
        game.doSomething("luca", "cervello");
        game.doOtherThing("H");
    }

}