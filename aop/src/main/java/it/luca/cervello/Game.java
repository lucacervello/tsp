package it.luca.cervello;

public class Game {

    @CheckParams
    public String doSomething(String name, String password) {
        return name + password;
    }
    
    @CheckParams(value = {"HELLO", "HI"})
    public String doOtherThing(String greeting) {
    		return greeting;
    }
}
