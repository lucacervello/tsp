package it.luca.cervello;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Aspect
public class AspectChecker {

    @Before("@annotation(checkParams)")
    public void checkParameters(JoinPoint joinPoint, CheckParams checkParams) throws Exception {
    		List<String> params = Arrays.stream(joinPoint.getArgs()).map(String.class::cast).collect(Collectors.toList());
    		if (params.stream().anyMatch(x -> Arrays.asList(checkParams.value()).contains(x))) {
    			throw new CheckerException();
    		}
    }
}
