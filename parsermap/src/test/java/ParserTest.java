import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Map;

public class ParserTest {

    JsonElement jsonElement;

    @Before
    public void setUp() throws Exception {
        jsonElement = new JsonParser().parse("{\"name\": { \"a\" : 1, \"b\" : 2 }}");
    }

    @Test @Ignore
    public void testHasMap() {
        Gson gson = new Gson();
        Type stringJsonPrimitive = new TypeToken<Map<String, JsonElement>>(){}.getType();
        Map<String, JsonElement> map = gson.fromJson(jsonElement, LinkedTreeMap.class);

        System.out.println(map.get("name").getAsJsonObject().get("a"));

    }
}