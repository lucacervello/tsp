Esercizio: Playing a Game.

Si desidera implementare una classe che rappresenti le carte da gioco (classe Card) con le seguenti proprietà:

    ogni carta è caratterizzata da un seme (istanza della classe Suit) e da un valore (istanza della classe Value); e
    non possono esistere due carte con la stessa coppia di Suit e Value.

I semi ammissibili sono i soliti quattro (clubs, diamonds, hearts e spades) ed i valori ammissibili sono asso, due, ... , dieci, fante, donna e re. Analogamente alle carte anche gli elementi di Suit e Value sono unici.

Lo svolgimento dell'esercizio prevede:

    una breve (nome e caratteristiche) introduzione dei pattern che si intende usare nella risoluzione del problema;

    l’implementazione delle classi Suit, Value e Card con i metodi che si ritiene necessari alla realizzazione di quanto richiesto;

    scrivere infine una classe it.luca.cervello.Game con main() come unico metodo che crei un po' di carte e dimostri che

    ∀c₁, c₂ є Card; c₁.suit = c₂.suit ∧ c₁.value = c₂.value ⇔ c₁ = c₂

cioè se due carte hanno lo stesso valore e lo stesso seme allora sono la stessa carta. Se ritenete necessario, implementate pure altre classi e metodi sempre nell'ottica della buona progettazione e del rispetto per i pattern che si intendono usare. È proibito usare le enum di Java 5.