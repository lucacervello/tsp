package builder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HeroTest {

    @Test
    public void testHero() {
        assertEquals("luca", new Hero.Builder().withName("luca").withUsername("cerve").build().getName());
    }
}