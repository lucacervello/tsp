package factory;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BlackSmithTest {

    @Test
    public void factoryTest() {
        BlackSmith blackSmith = new ElfBlackSmith();
        assertEquals(blackSmith.manufactureWeapon(WeaponType.AXE).getName(), "Elf axe");
        blackSmith = new OrchBlackSmith();
        assertEquals(blackSmith.manufactureWeapon(WeaponType.AXE).getName(), "Orch axe");
    }

}