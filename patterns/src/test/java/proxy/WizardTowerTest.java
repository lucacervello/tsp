package proxy;

import org.junit.Test;

public class WizardTowerTest {

    @Test
    public void testProxy() {
        WizardTowerProxy wizardTowerProxy = new WizardTowerProxy(new IvoryTower());
        wizardTowerProxy.enter(new Wizard("luca"));
        wizardTowerProxy.enter(new Wizard("marco"));
        wizardTowerProxy.enter(new Wizard("giovanni"));
        wizardTowerProxy.enter(new Wizard("Bad Wizard"));
    }
}