import abstractfactory.ElfKingdomFactory;
import abstractfactory.IKingdomFactory;
import abstractfactory.OrcishKingdomFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AbstractFactoryTest {
    @Test
    public void testAF() throws Exception {
        IKingdomFactory kingdomFactory = new ElfKingdomFactory();
        assertEquals(kingdomFactory.createArmy().getDescription(), "Elf army");
        assertEquals(kingdomFactory.createCastle().getDescription(), "Elf castle");
        assertEquals(kingdomFactory.createKing().getDescription(), "Elf king");
        kingdomFactory = new OrcishKingdomFactory();
        assertEquals(kingdomFactory.createArmy().getDescription(), "Orcish army");
        assertEquals(kingdomFactory.createCastle().getDescription(), "Orcish castle");
        assertEquals(kingdomFactory.createKing().getDescription(), "Orcish king");
    }
}
