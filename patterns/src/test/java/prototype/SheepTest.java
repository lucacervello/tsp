package prototype;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SheepTest {

    @Test
    public void testSheep() throws CloneNotSupportedException {
        Sheep sheep = new Sheep("Dolly", "standard");
        Sheep clonedSheep = sheep.clone();
        clonedSheep.setName("Jolly");
        assertNotEquals(sheep.getName(), clonedSheep.getName());
        assertEquals(sheep.getType(), clonedSheep.getType());
    }
}