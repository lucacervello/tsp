package singleton;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class IvoryTowerTest {

    @Test
    public void testSingleton() {
        assertTrue(IvoryTower.INSTANCE == IvoryTower.INSTANCE);
    }
}