package abstractfactory;

public interface ICastle {
    String getDescription();
}
