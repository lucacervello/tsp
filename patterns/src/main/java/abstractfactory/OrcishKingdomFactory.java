package abstractfactory;

public class OrcishKingdomFactory implements IKingdomFactory {
    public IKing createKing() {
        return new OrcishKing();
    }

    public ICastle createCastle() {
        return new OrcishCastle();
    }

    public IArmy createArmy() {
        return new OrcishArmy();
    }
}
