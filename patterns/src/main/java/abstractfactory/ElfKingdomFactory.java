package abstractfactory;

public class ElfKingdomFactory implements IKingdomFactory {
    public IKing createKing() {
        return new ElfKing();
    }

    public ICastle createCastle() {
        return new ElfCastle();
    }

    public IArmy createArmy() {
        return new ElfArmy();
    }
}
