package abstractfactory;

public interface IKingdomFactory {
    IKing createKing();
    ICastle createCastle();
    IArmy createArmy();
}
