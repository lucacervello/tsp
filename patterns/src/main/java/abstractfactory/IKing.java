package abstractfactory;

public interface IKing {
    String getDescription();
}
