package abstractfactory;

public interface IArmy {
    String getDescription();
}
