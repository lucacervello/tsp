package factory;

public class OrchBlackSmith implements BlackSmith {
    public Weapon manufactureWeapon(WeaponType type) {
        switch (type) {
            case AXE:
                return new OrchAxe();
            case SWORD:
                return new OrchSword();
            default:
                return null;
        }
    }
}
