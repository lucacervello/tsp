package factory;

public interface Weapon {
    String getName();
}
