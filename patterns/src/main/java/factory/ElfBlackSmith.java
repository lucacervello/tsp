package factory;

public class ElfBlackSmith implements BlackSmith {
    public Weapon manufactureWeapon(WeaponType type) {
        switch(type) {
            case AXE:
                return new ElfAxe();
            case SWORD:
                return new ElfSword();
            default:
                return null;
        }
    }
}
