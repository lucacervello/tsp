package factory;

public interface BlackSmith {
    Weapon manufactureWeapon(WeaponType type);
}
