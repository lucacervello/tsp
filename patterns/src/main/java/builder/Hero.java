package builder;

public class Hero {
    private final String name;
    private final String username;

    private Hero(Builder builder) {
        this.name = builder.name;
        this.username = builder.username;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public static class Builder {
        private String name;
        private String username;

        public Builder() {

        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Hero build() {
            return new Hero(this);
        }
    }
}
