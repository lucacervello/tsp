package proxy;

public class WizardTowerProxy implements WizardTower {

    private static final Integer NUM_WIZARDS_ALLOWED = 3;
    private Integer numWizards;
    private final WizardTower tower;

    public WizardTowerProxy(WizardTower wizardTower) {
        this.tower = wizardTower;
        numWizards = 0;
    }

    public void enter(Wizard wizard) {
        if (numWizards < NUM_WIZARDS_ALLOWED) {
            tower.enter(wizard);
            numWizards++;
        } else {
            System.out.println(wizard + " is not allowed to enter!");
        }
    }
}
