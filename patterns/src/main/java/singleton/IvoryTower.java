package singleton;

public enum IvoryTower {
    INSTANCE;
    public Object createObject() {
        return new Object();
    }
}
