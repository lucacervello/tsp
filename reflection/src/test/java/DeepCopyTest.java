import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DeepCopyTest {
    /**
     * it needs more work to works
     * @throws Exception
     */
    @Test @Ignore
    public void copy() throws Exception {
        TestObject testObject = new TestObject();
        testObject.setPrivateString("ciao");
        TestObject.setPrivateStaticString("hi");
        TestObject testObject1 = new TestObject();
        testObject1.setPrivateString("hallo");
        testObject1.publicString = "public";
        testObject.testObjectList.add(testObject1);
        assertThat(DeepCopy.copy(testObject))
                .isEqualToComparingFieldByField(testObject);
        assertThat(DeepCopy.copy(testObject))
                .isNotEqualTo(testObject);
    }

}