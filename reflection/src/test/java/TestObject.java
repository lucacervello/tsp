import java.util.ArrayList;
import java.util.List;

public class TestObject {
    private String privateString;
    public String  publicString;
    private static String privateStaticString;
    public static String publicStaticString;

    public List<TestObject> testObjectList;

    public TestObject() {
      testObjectList = new ArrayList<>();
    }

    public String getPrivateString() {
        return privateString;
    }

    public void setPrivateString(String privateString) {
        this.privateString = privateString;
    }

    public static String getPrivateStaticString() {
        return privateStaticString;
    }

    public static void setPrivateStaticString(String privateStaticString) {
        TestObject.privateStaticString = privateStaticString;
    }
}
