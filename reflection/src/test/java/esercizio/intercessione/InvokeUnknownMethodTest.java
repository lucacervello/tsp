package esercizio.intercessione;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public class InvokeUnknownMethodTest {

    @Test
    public void testUnknownMethod() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        System.out.println((Integer)InvokeUnknownMethod.run("esercizio.intercessione.Calculator", "add", "1", "2"));
    }

}