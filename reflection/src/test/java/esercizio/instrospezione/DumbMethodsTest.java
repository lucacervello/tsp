package esercizio.instrospezione;

import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class DumbMethodsTest {

   @Test
   public void testDumbMethod() {
       assertThat(Arrays.asList("wait",
                       "wait", "wait",
                       "equals", "toString",
                       "hashCode", "getClass",
                       "notify", "notifyAll"))
               .isEqualTo(DumbMethods.
                               getMethods("java.lang.Object"));

   }

}