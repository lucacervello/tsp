import java.lang.reflect.Field;

public class DeepCopy<T extends Object> {
    public static <T> T copy(T obj) {
        try {
            Class cls = obj.getClass();
            T objReturned = (T) cls.getConstructor((Class[])null).newInstance();
            do {
                Field[] fieldsDeclared = obj.getClass().getDeclaredFields();
                Field.setAccessible(fieldsDeclared, true);
                for (Field field : fieldsDeclared) {
                    if (field.getType().isPrimitive())
                        field.set(objReturned, field.get(obj));
                    else
                        field.set(objReturned, copy(field.get(obj)));
                }
                cls = cls.getSuperclass();
            } while (cls != Object.class);
            return objReturned;
        } catch (Exception e) {
            System.out.println("Exception : " + e);
            return null;
        }

    }
}
