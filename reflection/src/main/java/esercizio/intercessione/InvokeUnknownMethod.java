package esercizio.intercessione;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class InvokeUnknownMethod {
    public static void main(String... args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        run(args);
    }

    public static <T extends Object> T run(String... args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        String className = args[0];
        String methodName = args[1];
        List<Integer> argsList = Arrays.stream(args).skip(2).map(x -> Integer.valueOf(x).intValue()).collect(Collectors.toList());
        Class cls = Class.forName(className);
        Object newIntance = cls.newInstance();
        Method method = cls.getMethod(methodName, int.class, int.class);
//        Parameter[] parameters = method.getParameters();
//        Arrays.stream(parameters)
//                .map(Parameter::getType)
//                .filter(Class::isPrimitive)
//                .map(x -> (Object)x)
//                .toArray();
        Object result = method.invoke(newIntance, argsList.toArray());
        return (T) result;
    }
}
