package esercizio.testingfields;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Analizer {

    public static void analize(Object obj) throws IllegalAccessException {
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        Field fieldToModify = declaredFields[declaredFields.length - 1];
        fieldToModify.set(obj, fieldToModify.get(obj) + "passed!!!");
    }

    public static void main(String... args) throws IllegalAccessException {
        TestingFields testingFields = new TestingFields(7, 3.14);
        analize(testingFields);
        System.out.println(TestingFields.s);
    }
}
