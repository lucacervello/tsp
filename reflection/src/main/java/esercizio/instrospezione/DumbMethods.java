package esercizio.instrospezione;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DumbMethods {

    public static void main(String... args) {

        if (args.length > 0) {
            getMethods(args[0]).forEach(System.out::println);
        } else {
            System.out.println("First parameter must be given (a class name is appreciated)");
        }
    }

    public static List<String> getMethods(String className) {
        try {
            return Arrays.asList(Class.forName(className).getMethods()).stream().map(Method::getName).collect(Collectors.toList());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return Arrays.asList("A valid class name need to be provided");
        }
    }
}
