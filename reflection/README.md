
# ESERCIZI INSTROSPEZIONE

## Esercizio: Introspezione.

Per vedere come lavora la reflection in Java sviluppate la classe DumpMethods, contenente solo il main(), in modo che prenda come argomento da linea di comando (arg[0]) il nome di una classe e fornisca come output l'elenco dei metodi invocabili su un'istanza della classe.

## Esercizio: Intercessione.

Scrivere una classe InvokeUnknownMethod che invochi un metodo sconosciuto a compile-time tramite reflection. Supporre che il programma voglia invocare uno dei metodi della seguente classe:
    
    public class Calculator {
        public int add(int a, int b) { return a + b; }
        public int mul(int a, int b) { return a * b; }
        public double div(double a, double b) { return a / b; } public double neg(double a) { return -a; }
    }
    
ma che non ne conosca l'identità fino al momento dell'esecuzione; cioè il nome del metodo, il numero ed il tipo dei suoi parametri e la classe di appartenenza che dovranno essere dedotti dall'input. Un esempio di attivazione sarà:
    
    java InvokeUnknownMethod Calculator add 7 25

Suggerimento: usare la classe Pattern per dedurre il tipo dei parametri.

## Esercizio: Testing Fields.

Scrivere un programma che analizzi lo stato di una classe generica, per il testing usare la seguente classe:

ed una istanza creata con n=7 e val=3.14. Lo stato della nostra istanza è rappresentato dall'insieme dei valori di tutti i suoi attributi (statici e non, pubblici e privati).

Portare, sempre tramite reflection, il valore di s a testing ... passed!!!.

## Esercizio: Recognizing Elements.

Dato un insieme di classi Java, e due array contenenti i nomi delle classi (il primo) e i nomi degli attributi e delle costanti (il secondo) che dovrebbero essere presenti nelle classi (non si conosce l'associazione tra i due array, cioè non si sa in quale classe sono definiti i vari nomi, né se un nome rappresenta una costante o un attributo).

Scrivere un programma che verifichi che i nomi del secondo array siano tutti dichiarati in una delle classi elencate nel primo array e visualizzi a video la classe in cui sono dichiarate, se sono costanti o attributi, il tipo e se il qualificatore con cui sono dichiarate è corretto (gli attributi devono essere privati e le costanti pubbliche).

È necessario il primo array? ed il secondo? Potrebbe essere reso più generale?

## Esercizio: Control Flow Graph.

Annotare un programma in modo che successivamente sia possibile ricostruire il control flow graph delle chiamate sia statico (di tutto il programma) che dinamico (esclusivamente per le chiamate effettuate).

Per chi non lo sapesse il CFG è un grafo i cui nodi sono etichettati con le classi e gli archi coi metodi che permettono di passare da una classe all'altra (definizione molto semplificata), ovviamente sono ammessi cicli.

Nel caso specifico dell'esercizio, le informazioni utili saranno associate ad ogni singolo metodo e specificheranno quali metodi invoca e di quali classi. Detto questo, la definizione dell'annotazione con i giusti attributi la lascio a voi.

Scrivere il programma Java che estrae le annotazioni dal programma, costruisce e visualizza il CFG del codice esaminato. Il CFG è visualizzato tramite una visita depth-first che stampa le etichette dei nodi e degli archi man mano che la visita procede.

## Esercizio: Proxy.

Scrivere un meta-oggetto (tramite Proxy) da associare alle istanze della seguente classe:

e che visualizzi lo stato dell'oggetto prima e dopo l'esecuzione di ogni singolo metodo.
Esercizio: NestedCalls.

Scrivere un meta-oggetto (tramite Proxy) da associare alle istanze della seguente classe:

e che visualizzi per ogni invocazione il livello di annidamento. Suggerimento: uso sapiente di ereditarietà e polimorfismo.

## Esercizio: Class Loading.
scrivere un programma la cui esecuzione stampi a video quante classi di sistema (quelle nei package java.*) e user-defined sono state caricate. Ovviamente nel main() di test dovrete usare e quindi caricare un po' di classi. Come estensione considerate di far stampare anche il nome delle classi caricate.

Scrivere un programma java che sollevi una RuntimeException quando vengono usate le classi definite nei package java.lang e java.util. Usate il ClassLoader.

