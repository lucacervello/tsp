public class Suit {

    private final String suit;

    private Suit(String suit) {
        this.suit = suit;
    }

    public static final Suit CLUBS = new Suit("clubs");
    public static final Suit DIAMONDS = new Suit("diamonds");
    public static final Suit HEARTS = new Suit("hearts");
    public static final Suit SPADES = new Suit("spades");

    @Override
    public String toString() {
        return "Suit{" +
                "suit='" + suit + '\'' +
                '}';
    }
}

