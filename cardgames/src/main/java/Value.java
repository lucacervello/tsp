public class Value {
    private final Integer value;
    
    private Value(Integer value) {
        this.value = value;
    }
    
    public static final Value ACE = new Value(14);
    public static final Value KING = new Value(13);
    public static final Value QUEEN = new Value(12);
    public static final Value JACK = new Value(11);
    public static final Value TEN = new Value(10);
    public static final Value NINE = new Value(9);
    public static final Value EIGHT = new Value(8);
    public static final Value SEVEN = new Value(7);
    public static final Value SIX = new Value(6);
    public static final Value FIVE = new Value(5);
    public static final Value FOUR = new Value(4);
    public static final Value THREE = new Value(3);
    public static final Value TWO = new Value(2);

    @Override
    public String toString() {
        return "Value{" +
                "value=" + value +
                '}';
    }
}
