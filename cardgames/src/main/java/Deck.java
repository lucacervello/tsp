import java.util.HashSet;
import java.util.Set;

public class Deck {
    private static Set<Card> cards = new HashSet<>();

    public Card getCard(Suit suit, Value value) {
        Card card = new Card(suit, value);
        if (cards.contains(card)) {
            return cards.stream().filter(x -> x.equals(card)).findFirst().orElse(null);
        } else {
            cards.add(card);
            return card;
        }
    }

    public class Card {
        private final Value value;
        private final Suit suit;
        private Card(){
            this.suit = null;
            this.value = null;
        }
        private Card(Suit suit, Value value) {
            this.value = value;
            this.suit = suit;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Card card = (Card) o;

            if (value != null ? !value.equals(card.value) : card.value != null) return false;
            return suit != null ? suit.equals(card.suit) : card.suit == null;
        }

        @Override
        public int hashCode() {
            int result = value != null ? value.hashCode() : 0;
            result = 31 * result + (suit != null ? suit.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Card{" +
                    "value=" + value +
                    ", suit=" + suit +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Deck{" +
                cards.toString() +
                "}";
    }
}
