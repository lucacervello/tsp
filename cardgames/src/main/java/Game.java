
public class Game {
    public static void main(String... args) {
        Deck deck = new Deck();
        Deck.Card card1 = deck.getCard(Suit.CLUBS, Value.ACE);
        Deck.Card card2 = deck.getCard(Suit.CLUBS, Value.ACE);
        if (card1 == card2) {
            System.out.println("OK !!!");
        } else {
            System.out.println("Something is wrong");
        }
    }
}
